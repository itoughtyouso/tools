#!/usr/bin/env python

import asyncio
import websockets
from typing import List
#from queue import Queue

#q: Queue = Queue()
displays: List = []
controller = []  # type: ignore[var-annotated]
state = {
    'up': 0,
    'down': 0
}


async def controllerHandler(ws):
    async for message in ws:
        print(message)
        for display in displays:
            await display.send(message)


async def displayHandler(ws):
    displays.append(ws)
    async for message in ws:
        print(message)


async def handler(websocket):
    print('Handler called')
    message = await websocket.recv()
    if (message == 'startController'):
        print('starting Controller')
        controller.append(websocket)
        await controllerHandler(websocket)
    elif (message == 'startDisplay'):
        print('Adding Display')
        await displayHandler(websocket)


async def main():
    async with websockets.serve(handler, "", 8001):
        await asyncio.Future()  # run forever


if __name__ == "__main__":
    asyncio.run(main())
