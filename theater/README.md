# Aufzug

Eine kleine Webapp, die für ein Theaterstück geschrieben wurde. 

Ein Websocket-Server in Python nimmt Verbindungen von Displays und Controllern an. Über Controller lässt sich der Zustand der Displays ändern. Die Kommunikation erfolgt mittels [WebSockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API).
