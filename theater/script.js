var isController = false;
const wsUrl = "ws://" + window.location.hostname + ":8001";
console.log(wsUrl);
var socke;
var state = {
	'up': 0,
	'down': 0
}

function sendData() {
	socke.send(JSON.stringify(state));
}

function dataReceived(event) {
	console.log(event.data);
	data = JSON.parse(event.data);
	state = data;
	if (state['up'] == 0) {
		document.getElementById("arrowUp").classList.add("disabled");
	} else {
		document.getElementById("arrowUp").classList.remove("disabled");
	}
	if (state['down'] == 0) {
		document.getElementById("arrowDown").classList.add("disabled");
	} else {
		document.getElementById("arrowDown").classList.remove("disabled");
	}


}


function toggleUp() {
	console.log("Up Button clicked");
	this.classList.toggle('disabled');
	if (this.classList.contains('disabled')) {
		state['up'] = 0;
	} else {
		state['up'] = 1;
	}
	sendData();

}

function toggleDown() {
	console.log("Down Button clicked");
	this.classList.toggle('disabled');
	if (this.classList.contains('disabled')) {
		state['down'] = 0;
	} else {
		state['down'] = 1;
	}
	sendData();

}

function runasDisplay() {
	document.getElementById("ccselection").classList.toggle('hidden');
	document.getElementById("arrows").classList.toggle('hidden');
	isController = false;

	// Create WebSocket connection.
	socke = new WebSocket(wsUrl);
	setTimeout(function() {
		socke.send('startDisplay');
		socke.addEventListener("message", dataReceived);
	}, 500);
}

function runasController() {
	elem = document.documentElement
	if (elem.webkitRequestFullScreen) {
		elem.webkitRequestFullScreen();
	}
	document.getElementById("ccselection").classList.toggle('hidden');
	document.getElementById("arrows").classList.toggle('hidden');
    arrowUp.addEventListener("click", toggleUp);
    arrowDown.addEventListener("click", toggleDown);
	isController = true;

	// Create WebSocket connection.
	socke = new WebSocket(wsUrl);
	setTimeout(function() {
		socke.send('startController');
	}, 500);
}


document.addEventListener("readystatechange", (event) => {
  if (event.target.readyState === "complete") {
  	console.log("DOM ready");
    displaybutton.addEventListener("click", runasDisplay);
    controllerbutton.addEventListener("click", runasController);
  }
});

