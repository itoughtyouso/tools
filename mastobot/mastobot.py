#!/usr/bin/env python3

#
# Bot, der Beiträge mit dem Tag #didldo auf bildung.social sucht und retrötet.
#
# Autor: Frank Zimper
# 

import feedparser
import requests
import json
import sys

# Konfiguration einlesen
with open('mastobot.conf', 'r') as configfile:
    config = json.load(configfile)

url = 'https://bildung.social/api/v1/statuses/{:d}/reblog'
auth = {'Authorization': f"Bearer {config['auth_code']}"}

# ID des zuletzt retröteten Tröts :-)
latestId = config['lastid']


# Suche Posts mit dem Tag #didldo
Feed = feedparser.parse('https://bildung.social/tags/didldo.rss')

# Wenn der Post nach dem bisher letzten kam: Repost.
for entry in Feed.entries:
    ts = entry.published_parsed
    idNum = int(entry.id.split('/')[-1:][0])
    if idNum > config['lastid']:

        if idNum > latestId:
            latestId = idNum

        print(url.format(idNum))
        # post it
        result = requests.post(url.format(idNum), headers=auth)

        print(f'{entry.link}, {ts.tm_year}-{ts.tm_mon}-{ts.tm_mday} {ts.tm_hour}:{ts.tm_min:02}:{ts.tm_sec:02}')


# Ändere lastid in Konfigurationsdatei auf zuletzt geposteten Tröt
config['lastid'] = latestId
with open('mastobot.conf', 'w') as configfile:
    json.dump(config, configfile, indent=4)
