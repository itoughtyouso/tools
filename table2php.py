#!/usr/bin/env python3

from fileinput import input

def getterSetter(typ, field):
    result = ''
    constructorLine = ''
    constructorTemplate = "        $this->set{N}($data['t_{n}']);\n";
    constructorTemplateDate = "        $this->set{N}FromString($data['t_{n}']);\n";
    jsonSerializeTemplate = " " * 12 + "'{n}' => $this->get{N}(),\n";

    # Template für Standard Setter und Getter
    template = '''
    public function set{N}({typ} ${n})
    {{
        $this->{n} = ${n};
    }}

    public function get{N}(): {typ}
    {{
        return $this->{n};
    }}

    '''

    # zus. Template für DateTime Setter
    template2 = '''
    public function set{N}FromString(${n}, $tz = 'UTC')
    {{
        $this->{n} = date_create(${n}, new DateTimeZone($tz));
    }}

    '''
       
    Name = field.title()
    result = template.format(typ=typ, n=field, N=Name)

    if (typ in ['DateTime']):
        result += template2.format(n=field, N=Name)
        constructorLine = constructorTemplateDate.format(n=field, N=Name)
    else:
        constructorLine = constructorTemplate.format(n=field, N=Name)
    
    jsonSerializeLine = jsonSerializeTemplate.format(n=field, N=Name)
    return {'result': result, 'constructor': constructorLine, 'jsonSerialize': jsonSerializeLine}


constructor = "    function __construct(array $data)\n    {\n"
jsonSerialize = "    public function JsonSerialize(): mixed\n    {\n        return [\n"

for line in input(encoding="utf-8"):
    line = line.strip().lower()
    line = line.replace('[', '')
    line = line.replace(']', '')
    (field, typ, other) = line.split(' ', 2)

    if (typ.startswith('varchar')):
        start = typ.find('(')
        end = typ.find(')')
        size = typ[start+1:end]
        typ = 'string'
        print(f'    #[Assert\\Length(max: {size})]')

    if (typ == 'datetime'):
        typ = 'DateTime'
    if (field.startswith('t_')):
        field = field[2:]

    if ('not null' in other.lower()):
        print('    #[Assert\\NotNull]')

    print(f'    private {typ} ${field};')
    print()

    code = getterSetter(typ, field)
    print (code['result'])

    constructor += code['constructor']
    jsonSerialize += code['jsonSerialize']

constructor += "    }\n\n"
jsonSerialize += "        ];\n    }\n"

print (constructor)
print (jsonSerialize)
print ("}\n")