from requests import request
from hashlib import sha1

""" Example program to check haveibeenpwned.com for a given password
    Copyright (C) 2023  Frank Zimper

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 """

baseUrl = 'https://api.pwnedpasswords.com/range/'
user_agent = "School Password Checker"

pw2check = input('Enter password to check: ')

# get SHA-1 hash of password to check
hash = sha1(pw2check.encode('utf-8'))

# for privacy reasons, you don't send the password to haveibeenpwned.
# neither do you send the full hash, but only the first 5 hex digits.
# the API then returns a list of hashes that have been found in breached
# sites.
url = baseUrl + hash.hexdigest()[:5]
response = request('GET', url, headers = {'user-agent': user_agent})

# The response of the API call is a string containing a list 
# of password hashes (minus the first 5 characters) and the number
# of times they have been found in breached password lists

# split result into a list of entries
resultList = response.content.decode("utf-8").split('\r\n')

for result in resultList:
    (hashPart, number) = result.split(':')
    
    if hashPart == hash.hexdigest()[5:].upper():
        print (f'Breached! This password has been used {number} times.')
        exit()

print ("This password has not been pwned ... yet")
    

